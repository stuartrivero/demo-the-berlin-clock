package com.stuartrivero.clock;

import static java.lang.String.format;

public class TheBerlinClockGenerator {

    public TheBerlinClock from(int hours, int minutes, int seconds) {
        validateTime(hours, minutes, seconds);
        boolean secondsIsEvent = seconds % 2 == 0;
        return new TheBerlinClock(
                secondsIsEvent,
                hours / 5,
                hours % 5,
                minutes / 5,
                minutes % 5);
    }

    private void validateTime(int hours, int minutes, int seconds) {
        if (hours > 23 || hours < 0 || minutes > 59 || minutes < 0 || seconds < 0 || seconds > 59) {
            throw new IllegalArgumentException(
                    format("Invalid time values: Hours=%s, Minutes=%s, Seconds=%s ", hours, minutes, seconds));
        }
    }

}
