package com.stuartrivero.clock;

public class TheBerlinClock {
    private final boolean topLightOn;
    private final int fiveHourLights;
    private final int oneHourLights;
    private final int fiveMinLights;
    private final int oneMinLights;

    public TheBerlinClock(boolean topLightOn, int fiveHourLights, int oneHourLights, int fiveMinLights, int oneMinLights) {
        this.topLightOn = topLightOn;
        this.fiveHourLights = fiveHourLights;
        this.oneHourLights = oneHourLights;
        this.fiveMinLights = fiveMinLights;
        this.oneMinLights = oneMinLights;
    }

    public boolean isTopLightOn() {
        return topLightOn;
    }

    public int getFiveHourLights() {
        return fiveHourLights;
    }

    public int getOneHourLights() {
        return oneHourLights;
    }

    public int getFiveMinLights() {
        return fiveMinLights;
    }

    public int getOneMinLights() {
        return oneMinLights;
    }

    @Override
    public String toString() {
        return "TheBerlinClock{" +
                "topLightOn=" + topLightOn +
                ", fiveHourLights=" + fiveHourLights +
                ", oneHourLights=" + oneHourLights +
                ", fiveMinLights=" + fiveMinLights +
                ", oneMinLights=" + oneMinLights +
                '}';
    }
}
