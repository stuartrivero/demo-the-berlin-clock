package com.stuartrivero.clock;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class TheBerlinClockGeneratorTest {

    private final TheBerlinClockGenerator clockGenerator = new TheBerlinClockGenerator();

    @DisplayName("IllegalArgumentException thrown for invalid times")
    @ParameterizedTest(name = "Time for \"{0}:{1}:{2}\" should throw an exception")
    @CsvSource({"-1, 0, 0", "24, 0, 0", "0, -1, 0", "0, 60, 0", "0, 0, -1", "0, 0, 60"})
    void invalidArgumentExceptionThrown(int hours, int minutes, int seconds) {
        assertThrows(IllegalArgumentException.class, () -> clockGenerator.from(hours, minutes, seconds));
    }

    @DisplayName("When the clock has even seconds then the top light is on")
    @Test
    void evenSecondsHasLightOn() {
        TheBerlinClock clock = clockGenerator.from(10, 31, 10);
        assertTrue(clock.isTopLightOn());
    }

    @DisplayName("When the clock has odd seconds then the top light is off")
    @Test
    void oddSecondsHasLightOff() {
        TheBerlinClock clock = clockGenerator.from(10, 31, 11);
        assertFalse(clock.isTopLightOn());
    }

    @DisplayName("Number of 5 hour blocks")
    @ParameterizedTest(name = "\"{0}\" should be {1} blocks")
    @CsvSource({"0, 0", "1, 0", "5, 1", "6, 1", "10,2", "14,2", "15,3", "16,3", "19,3", "20,4", "23,4"})
    void numberOf5HourBlocks(int hour, int expectedNum5HourBlocks) {
        TheBerlinClock clock = clockGenerator.from(hour, 0, 0);
        assertEquals(expectedNum5HourBlocks, clock.getFiveHourLights());
    }

    @DisplayName("Number of hour blocks")
    @ParameterizedTest(name = "\"{0}\" should be {1} blocks")
    @CsvSource({"0, 0", "1, 1", "5, 0", "6, 1", "9, 4", "10, 0", "14, 4", "15, 0", "16, 1", "19, 4", "20, 0", "23, 3"})
    void numberOfHourBlocks(int hour, int expectedNumHourBlocks) {
        TheBerlinClock clock = clockGenerator.from(hour, 0, 0);
        assertEquals(expectedNumHourBlocks, clock.getOneHourLights());
    }

    @DisplayName("Number of 5 minute blocks")
    @ParameterizedTest(name = "\"{0}\" should be {1} blocks")
    @CsvSource({"0, 0", "5, 1", "10, 2", "15, 3", "16, 3", "20, 4", "24, 4", "54, 10", "59, 11"})
    void numberOf5MinuteBlocks(int minutes, int expectedNumBlocks) {
        TheBerlinClock clock = clockGenerator.from(0, minutes, 0);
        assertEquals(expectedNumBlocks, clock.getFiveMinLights());
    }

    @DisplayName("Number of 1 minute blocks")
    @ParameterizedTest(name = "\"{0}\" should be {1} blocks")
    @CsvSource({"0, 0", "4, 4", "5, 0", "10, 0", "15, 0", "16, 1", "20, 0", "24, 4", "54, 4", "59, 4"})
    void numberOfMinuteBlocks(int minutes, int expectedNumBlocks) {
        TheBerlinClock clock = clockGenerator.from(0, minutes, 0);
        assertEquals(expectedNumBlocks, clock.getOneMinLights());
    }

    @DisplayName("When the time is 10:31:22 the Clock values are set")
    @Test
    void completeClockShown() {
        TheBerlinClock clock = clockGenerator.from(10, 31, 22);
        assertTrue(clock.isTopLightOn());
        assertEquals(2, clock.getFiveHourLights());
        assertEquals(0, clock.getOneHourLights());
        assertEquals(6, clock.getFiveMinLights());
        assertEquals(1, clock.getOneMinLights());
    }
}