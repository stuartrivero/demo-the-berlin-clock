# demo-the-berlin-clock

[The Berlin clock](https://en.wikipedia.org/wiki/Mengenlehreuhr) is a clock which shows the time with coloured lights.
 
The top lamp blinks to show seconds - it is illuminated on even seconds and off on odd seconds.
The next two rows represent hours. The upper row represents 5 hour blocks and is made up of 4 red lamps. The lower row represents 1 hour blocks and is also made up of 4 red lamps.
The final two rows represent the minutes. The upper row represents 5 minute blocks, and is made up of 11 lamps- every third lamp is red, the rest are yellow. The bottom row represents 1 minute blocks, and is made up of 4 yellow lamps.
So the clock in the picture above shows the time as 10:31 (and an even number of seconds).